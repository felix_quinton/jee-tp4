package ex1;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.Session;


import java.util.List;
import java.util.Objects;


public class EmployeeDAO {


    private final SessionFactory sessionFactory = HibernateUtils.createSessionFactory();
    public EmployeeDAO() {

    }


    public boolean create(String firstName, String lastName, int salary) {
        Employee employee = new Employee(firstName, lastName, salary);
        Objects.requireNonNull(employee);
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.persist(employee);
        session.getTransaction().commit();
        session.close();

        return true;
    }


    public boolean delete(long id) {
        Session session = this.sessionFactory.openSession();
        Employee employee = session.get(Employee.class, id);
        session.beginTransaction();
        session.delete(employee);
        session.getTransaction().commit();
        session.close();

        return true;
    }

    public boolean updateSalary(long id) {
        Session session = this.sessionFactory.openSession();
        Employee employee = session.get(Employee.class, id);
        if(employee.getSalary() < 1000) {
            employee.setSalary(employee.getSalary() + 100);
        }else{
            employee.setSalary(employee.getSalary() + (int)(employee.getSalary() * 0.1));
        }
        session.beginTransaction();
        session.merge(employee);
        session.getTransaction().commit();
        session.close();

        return true;
    }


    public Employee get(long id) {
        Session session = this.sessionFactory.openSession();
        Employee employee = session.get(Employee.class, id);
        session.close();
        return employee;
    }


    public List<Employee> getAll() {
        Session session = this.sessionFactory.openSession();
        List<Employee> employees = session.createQuery("from Employee", Employee.class).getResultList();
        session.close();
        return employees;
    }

    public List<Employee> getAllByFirstName(String firstName) {
        Session session = this.sessionFactory.openSession();
        Query<Employee> req = session.createQuery("from Employee e where e.firstName = :name", Employee.class);
        req.setParameter("name", firstName);
        List<Employee> employees = req.getResultList();
        session.close();
        return employees;
    }
}
