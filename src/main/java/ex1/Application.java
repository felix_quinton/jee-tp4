package ex1;

import org.hibernate.SessionFactory;

import java.util.List;

public class Application {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtils.createSessionFactory();
        EmployeeDAO service = new EmployeeDAO();

        service.create("Jean", "Moran", 400);
        service.create("Jean", "Dylan", 600);
        service.create("Lisa","Simpson", 100);
        service.create("Marge", "Simpson", 1000);
        service.create("Homer", "Simpson", 450);

        service.delete(3);
        List<Employee> employees = service.getAll();
        for(Employee employee: employees) {
            if(employee.getSalary() < 550) {
                service.updateSalary(employee.getId());
            }
        }

        List<Employee> employees_firstName = service.getAllByFirstName("Bob");
        for(Employee employee: employees_firstName) {
            System.out.println(employee.toString());
        }
    }
}